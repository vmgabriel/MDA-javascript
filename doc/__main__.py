#!/urs/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import os, time
from os.path import dirname, join
from textx.metamodel import metamodel_from_file
from textx.export import metamodel_export, model_export

this_folder = dirname(__file__)

class SimpleType(object):
 """
 We are registering user SimpleType class to support
 simple types (integer, string) in our entity models
 Thus, user doesn't need to provide integer and string
 types in the model but can reference them in attribute types nevertheless.
 """
 def __init__(self, parent, name):
  self.parent = parent
  self.name = name

 def __str__(self):
  return self.name

def get_entity_mm(debug=False):
 """
 Builds and returns a meta-model for Entity language.
 """

 # Built-in simple types
 # Each model will have this simple types during reference resolving but
 # these will not be a part of `types` list of EntityModel.
 type_builtins = {
  'integer': SimpleType(None, 'integer'),
  'string': SimpleType(None, 'string'),
  'float': SimpleType(None, 'float'),
  'time': SimpleType(None,'time')
 }
 entity_mm = metamodel_from_file(join(this_folder, 'gramatica.tx'),
                                 classes=[SimpleType],
                                 builtins=type_builtins,
                                 debug=debug)
 return entity_mm


def main(debug=False):
 entity_mm = get_entity_mm(debug)
 nombre_carpeta = "diagrams"


 # Export to .dot file for visualization
 out_folder = join(this_folder, nombre_carpeta)
 if not os.path.exists(out_folder):
  os.mkdir(out_folder)
  metamodel_export(entity_mm, join(out_folder, 'dslDiagram.dot'))

  # Build Person model from person.ent file
  modelo_app = entity_mm.model_from_file(join(this_folder, 'modelo.ent'))

  # Export to .dot file for visualization
  model_export(modelo_app,  join(out_folder, 'modelo.dot'))
 else:
  print "Ya existe la carpeta, por favor borrela para poder compilar"


if __name__ == "__main__":
 main()
